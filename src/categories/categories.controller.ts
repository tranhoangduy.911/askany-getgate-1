import {
    Body,
    Controller,
    Get,
    HttpStatus,
    Post,
    Req,
    Res,
    UseGuards,
} from '@nestjs/common';
import { CategoriesService } from './categories.service';
@Controller('categories')
export class CategoriesController {
    constructor(private readonly CategoriesService: CategoriesService) { }
    @Get()
    async getAll(@Req() req, @Res() res, @Body() body) {
        const signalGet = await this.CategoriesService.getAll();
        const code = signalGet?.error ? HttpStatus.BAD_REQUEST : HttpStatus.OK;
        return res.status(code).json(signalGet);
    }

}
