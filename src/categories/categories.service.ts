import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class CategoriesService {
    constructor(
        @Inject('CATEGORIES_SERVICE') private client: ClientProxy,
    ) { }

    async getAll() {
        const signalGet = await lastValueFrom(
          this.client.send('getAllCategories', {}),
        );
        // console.log(signalGet);
        return signalGet;
      }


}
