import {
    Body,
    Controller,
    Get,
    HttpStatus,
    Post,
    Req,
    Res,
    UseGuards,
} from '@nestjs/common';
import { ArticlesService } from './articles.service';
@Controller('articles')
export class ArticlesController {
    constructor(private readonly ArticlesService: ArticlesService) { }
    @Get()
    async getAll(@Req() req, @Res() res, @Body() body) {
        const signalGet = await this.ArticlesService.getAll();
        const code = signalGet?.error ? HttpStatus.BAD_REQUEST : HttpStatus.OK;
        return res.status(code).json(signalGet);
    }

}
