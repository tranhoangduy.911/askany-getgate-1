import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class ArticlesService {
    constructor(
        @Inject('ARTICLES_SERVICE') private client: ClientProxy,
    ) { }

    async getAll() {
        const signalGet = await lastValueFrom(
          this.client.send('getAllArticles', {}),
        );
        // console.log(signalGet);
        return signalGet;
      }


}
